package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.mygdx.game.screens.SplashScreen;

public class GameProject extends Game {
	
	public final static int WIDTH = 576;
	public final static int HEIGHT = 1024;
	
	
	@Override
	public void create () {
		this.setScreen(new SplashScreen(this));
	}
	
}
