package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Background extends Rectangle {
	
	private Texture texture;
	
	public Background(Texture texture) {
		this.texture = texture;
	}
	
	public void draw (SpriteBatch batch) {
		batch.draw(texture, x, y);
	}

}
