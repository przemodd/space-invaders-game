package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.GameProject;

public class DeadScreen extends AbstractScreen {

	private Texture deadImg;
	private int score;
	private BitmapFont font;
	
	public DeadScreen(GameProject game, int score) {
		super(game);
		this.score = score;
		init();
	}

	private void init() {
		deadImg = new Texture("deadImg.png");
		
        font = new BitmapFont();
		
		font.getData().setScale(4);
		
		font.setColor(Color.GREEN);
	}
	
	private void screenChange() {
		if(Gdx.input.isKeyPressed(Keys.ENTER)) {
			game.setScreen(new GameplayScreen(game));
		}
	}
	
	
	public void render(float delta) {
		super.render(delta);
		
		screenChange();
		
		spriteBatch.begin();
		spriteBatch.draw(deadImg, 0, 0);
		font.draw(spriteBatch, String.valueOf(score), game.WIDTH/2 - 20, game.HEIGHT - 600);
		spriteBatch.end();
		
	}
}
