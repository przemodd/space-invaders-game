package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.Background;
import com.mygdx.game.BottomBorder;
import com.mygdx.game.Enemy;
import com.mygdx.game.Enemy2;
import com.mygdx.game.GameProject;
import com.mygdx.game.ShipPlayer;

public class GameplayScreen extends AbstractScreen {
	
	private Texture backgroundTexture, playerTexture, enemyTexture, enemy2Texture, deadImg, borderTexture;
	private Array<Background> backgroundArray;
	private Array<Enemy> enemyArray;
	private Array<Enemy2> enemy2Array;
	private Enemy enemy;
	private Enemy2 enemy2;
	private ShipPlayer player;
	private BottomBorder border;
	private Background background;
	private OrthographicCamera camera;
	private float obstacleSpeed;
	private float playerSpeed = 360;
	private boolean canAccel;
	private float accel = 1;
	public int score = 0;
	private BitmapFont font;
	
	SpriteBatch batch;

	public GameplayScreen(GameProject game) {
		super(game);
		loadData();
		init();
	}

	private void backgroundScrolling() {
		for (Background b : backgroundArray) {
			b.y -= 200 * accel * Gdx.graphics.getDeltaTime();
			b.draw(batch);
		}

	}

	private void enemyScrolling() {
		for (Enemy e : enemyArray) {
			e.y -= 300 * accel * Gdx.graphics.getDeltaTime();
			e.draw(batch);
			if (player.overlaps(e))
				game.setScreen(new DeadScreen(game, score));
			
			if (border.overlaps(e)) {
				score++;
				e.x = 1000;
			}
		}
		
	}
	
	private void enemy2Scrolling() {
		for (Enemy2 e : enemy2Array) {
			e.y -= 300 * accel * Gdx.graphics.getDeltaTime();
			e.draw(batch);
			if (player.overlaps(e))
				game.setScreen(new DeadScreen(game, score));
			
			if (border.overlaps(e)) {
				score++;
				e.x = 10000;
			}
		}
	}
	
	private void accelerate() {
		accel += 0.0002;
	}

	private void loadData() {
		backgroundTexture = new Texture("background.png");
		playerTexture = new Texture("spaceship.png");
		enemyTexture = new Texture("laser.png");
		enemy2Texture = new Texture("laser2.png");
		borderTexture = new Texture("border.png");
	}

	private void update() {
		handleInput();
		blockMovement();
		accelerate();
		camera.update();
	}

	private void blockMovement() {
		if (player.x < 0) {
			player.x = 1;
		}
		if (player.x > 576 - 64) {
			player.x = 576 - 65;
		}
	}

	private void handleInput() {
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			player.x -= playerSpeed * accel * 0.7 * Gdx.graphics.getDeltaTime();
		}

		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			player.x += playerSpeed * accel * 0.7 * Gdx.graphics.getDeltaTime();
		} 
	}

	private void init() {
		
		batch = new SpriteBatch();
		
		border = new BottomBorder(borderTexture);
		
		font = new BitmapFont();
		
		font.getData().setScale(2);
		
		font.setColor(Color.GREEN);

		camera = new OrthographicCamera(GameProject.WIDTH, GameProject.HEIGHT);

		background = new Background(backgroundTexture);

		backgroundArray = new Array<Background>();

		for (int i = 0; i < 1000; i++) {
			Background b = new Background(backgroundTexture);
			b.x = 0;
			b.y = 1280 * i;
			backgroundArray.add(b);
		}

		enemyArray = new Array<Enemy>();

		for (int i = 3; i < 1000; i++) {
			Enemy e = new Enemy(enemyTexture);
			e.x = MathUtils.random(10, 567);
			e.y = (400 +  MathUtils.random(100)) * i;
			enemyArray.add(e);
		}
		
		enemy2Array = new Array<Enemy2>();

		for (int i = 3; i < 1000; i++) {
			Enemy2 e = new Enemy2(enemy2Texture);
			e.x = MathUtils.random(10, 567);
			e.y = (300 +  MathUtils.random(100)) * i;
			enemy2Array.add(e);
		}

		player = new ShipPlayer(playerTexture);
		player.x = GameProject.WIDTH / 2 - 32;
		player.y += 50;
	}

	@Override
	public void render(float delta) {
		super.render(delta);

		update();

		batch.begin();

		backgroundScrolling();
		enemyScrolling();
		enemy2Scrolling();
		
		border.draw(batch);

		font.draw(batch, String.valueOf(score), game.WIDTH/2, game.HEIGHT - 20);
		
		player.draw(batch);

		batch.end();
	}
}
