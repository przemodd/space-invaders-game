package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.GameProject;

public class SplashScreen extends AbstractScreen {
	
	private Texture splashImg;
	
	public SplashScreen(GameProject game) {
		super(game);
		init();
	}
	
	private void init() {
		splashImg = new Texture("splashImg.jpg");
	}
	
	private void screenChange() {
		if(Gdx.input.isKeyPressed(Keys.ENTER)) {
			game.setScreen(new GameplayScreen(game));
		}
	}
	
	
	public void render(float delta) {
		super.render(delta);
		
		screenChange();
		
		spriteBatch.begin();
		spriteBatch.draw(splashImg, 0, 0);
		spriteBatch.end();
		
	}
	
	

}
