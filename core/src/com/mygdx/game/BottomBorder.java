package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class BottomBorder extends Rectangle {
	private Texture texture;

	public BottomBorder(Texture texture) {
		this.texture = texture;
		this.height = 5;
		this.width = 1000;
	}

	public void draw(SpriteBatch batch) {
		batch.draw(texture, 0, -24);
	}
}
